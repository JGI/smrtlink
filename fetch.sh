#!/bin/bash
#
# Set DEPLOY_* variables in the host environment running the runner,
# not in the gitlab environment.
#

set -e

VERSION=$1
if [[ "${VERSION}" == "" ]]; then
    echo "Not deploying because VERSION is empty."
    exit 1
fi
echo "Attempt to build version $VERSION"

if [[ "${DEPLOY_KEY}" == "" ]]; then
    echo "Not building because DEPLOY_KEY is empty."
    exit 1
fi

if [[ "${DEPLOY_HOST}" == "" ]]; then
    echo "Not building because DEPLOY_HOST is empty."
    exit 1
fi

if [[ "${DEPLOY_USER}" == "" ]]; then
    echo "Not building because DEPLOY_USER is empty."
    exit 1
fi

if [[ "${DEPLOY_TOKEN}" == "" ]]; then
    echo "Not building because DEPLOY_TOKEN is empty."
    exit 1
fi

deploy_token=my-ssh-deploy-token
[ -f $deploy_token ] && rm -f $deploy_token
echo $DEPLOY_TOKEN > $deploy_token

deploy_key=my_ssh-deploy-key
[ -f $deploy_key.enc ] && rm -f $deploy_key.enc
[ -f $deploy_key     ] && rm -f $deploy_key
echo $DEPLOY_KEY | tr ' ' "\n" > $deploy_key.enc
openssl enc -d -aes-256-cbc -a -kfile $deploy_token -in $deploy_key.enc > $deploy_key
chmod 0400 $deploy_key

eval $(ssh-agent)
ssh-add $deploy_key

ssh -o "UserKnownHostsFile /dev/null" \
    -o "StrictHostKeyChecking no" \
    ${DEPLOY_USER}@${DEPLOY_HOST} \
    -C \(cd /usr/common/jgi/utilities/smrtlink/$VERSION\; tar cf - .\) \
     | tee smrtlink.tar >/dev/null
ls -lh smrtlink.tar
