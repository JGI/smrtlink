FROM debian:jessie

ARG BUILD_NAME=$BUILD_NAME
ARG BUILD_REF=$BUILD_REF

LABEL Maintainer "Tony Wildish wildish@lbl.gov" \
      BUILD_NAME $BUILD_NAME \
      BUILD_REF $BUILD_REF

ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update -y && \
    apt-get install -y wget ca-certificates --no-install-recommends && \
    apt-get purge -y --auto-remove

# ADD smrtlink.tar /usr/common/jgi/utilities/smrtlink/4.0.0.190159
ADD smrtlink.tar /smrtlink/
CMD [ "/bin/bash" ]
